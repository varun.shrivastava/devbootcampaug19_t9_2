# Evolution of Trust

### Step 1:
    
    - Model the game for 2 console players
    - Display & verify the output for each round.
    
#### Rules:
    
    - I cheat and other cooperate, I get +3 points, they loose 1 point.
    - I cheat they cheat, Nobody gets any point
    - I cooperate and they cheat, they will get 3 points and we will loose 1 points
    
### Step 2:
Model the game between "Always Co-operate" and "Always Cheat"
Display and verify the output for each round (total 5 round)

    - New Player Type - Always Cooperates
    - New Player Type - Always Cheat
    
### Step 4:
Model the game between "Copy Cat" and Grudger
I'll start with cooperating but if you ever cheat than I will be cheating.

    - New Player Type: Grudger
    
### Step 5:
First: I analyze you. I start: Cooperate, Cheat, Cooperate, Cooperate. 
If you cheat back, I'll act like CopyCat. If you never cheat back, I'll act
like Always Cheat, to exploit you. Elementary, my dear Watson.

    - New Player Type: Detective