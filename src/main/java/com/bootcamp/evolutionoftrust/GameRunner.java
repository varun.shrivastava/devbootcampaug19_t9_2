package com.bootcamp.evolutionoftrust;

import java.io.PrintStream;

public class GameRunner {

    public static void main(String [] args) {
//        Scanner scanner = new Scanner(System.in);
//        Player playerOne = new Player(Behaviour.normal(new Scanner(System.in)));
//        Player playerTwo = new Player(Behaviour.normal(new Scanner(System.in)));
//        Player alwaysCooperate = new Player(Behaviour.cooperate());
//        Player alwaysCheat = new Player(Behaviour.cheat());

        ObserveBehaviour copyBehaviour = Behaviour.copy();
        Player copyPlayer = new Player(copyBehaviour);
        Player grudgePlayer = new Player(Behaviour.grudge());
        Machine machine = new Machine();
        PrintStream console = System.out;
        Game game = new Game(grudgePlayer, copyPlayer, machine, 5, console);
        game.subscribe(copyBehaviour);
        game.start();
    }
}
