package com.bootcamp.evolutionoftrust;

class AlwaysCooperate implements Behaviour {

    @Override
    public Move move() {
        return Move.COOPERATE;
    }
}
