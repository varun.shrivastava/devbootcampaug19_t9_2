package com.bootcamp.evolutionoftrust;

public class GrudgeBehaviour extends ObserveBehaviour {

    public Move move() {
        return this.move;
    }

    @Override
    public void notifyOf(Move move1, Move move2) {
        if (this.move == Move.CHEAT)
            return;
        Move opponentMove = this.move() == move1 ? move2 : move1;
        if (opponentMove == Move.CHEAT) {
            this.move = Move.CHEAT;
        }
    }
}
