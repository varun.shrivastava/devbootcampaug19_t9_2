package com.bootcamp.evolutionoftrust;

class AlwaysCheat implements Behaviour {

    @Override
    public Move move() {
        return Move.CHEAT;
    }
}
