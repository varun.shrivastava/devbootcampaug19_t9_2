package com.bootcamp.evolutionoftrust;

class NormalBehaviour implements Behaviour {

    private final Move move;

    public NormalBehaviour(String move) {
        this.move = Move.valueOf(move);
    }

    @Override
    public Move move() {
        return move;
    }
}