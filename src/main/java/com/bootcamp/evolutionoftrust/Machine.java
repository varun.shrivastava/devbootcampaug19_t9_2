package com.bootcamp.evolutionoftrust;

import java.util.HashMap;
import java.util.Map;

public class Machine {

    private static final Map<String, String> rules = new HashMap<>(){
        {
            put("CHEAT:COOPERATE", "3,-1");
            put("COOPERATE:CHEAT", "-1,3");
            put("CHEAT:CHEAT", "0,0");
            put("COOPERATE:COOPERATE", "2,2");
        }
    };

    public String calculateScore(Move playerOneMove, Move playerTwoMove) {
        return Machine.rules.get(playerOneMove.toString().concat(":").concat(playerTwoMove.toString()));
    }
}
