package com.bootcamp.evolutionoftrust;

import java.util.Scanner;

public interface Behaviour {

    static Behaviour normal(Scanner scanner) {
        return new NormalBehaviour(scanner.nextLine());
    }

    static Behaviour cheat() {
        return new AlwaysCheat();
    }

    static Behaviour cooperate() {
        return new AlwaysCooperate();
    }

    static ObserveBehaviour copy() {
        return new AlwaysCopy();
    }

    static Behaviour grudge() {
        return new GrudgeBehaviour();
    }

    Move move();
}



