package com.bootcamp.evolutionoftrust;

class AlwaysCopy extends ObserveBehaviour {

    @Override
    public Move move() {
        return this.move;
    }

    @Override
    public void notifyOf(Move move1, Move move2) {
        this.move = this.move() == move1 ? move2 : move1;
    }
}
