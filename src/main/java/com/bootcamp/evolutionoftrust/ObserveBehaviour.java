package com.bootcamp.evolutionoftrust;

public abstract class ObserveBehaviour implements Behaviour {

    protected Move move = Move.COOPERATE; // default behaviour

    abstract void notifyOf(Move move1, Move move2);
}
