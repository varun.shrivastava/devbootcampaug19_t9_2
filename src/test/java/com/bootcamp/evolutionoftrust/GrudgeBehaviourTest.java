package com.bootcamp.evolutionoftrust;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GrudgeBehaviourTest {

    private GrudgeBehaviour grudgeBehaviour;

    @Before
    public void shouldInitiateGrudgeBehaviourInstance() {
        grudgeBehaviour = new GrudgeBehaviour();
        Assert.assertNotNull(grudgeBehaviour);
    }

    @Test
    public void shouldCooperateForTheFirstTime() {
        Assert.assertEquals(Move.COOPERATE, grudgeBehaviour.move());
    }

    @Test
    public void shouldReturnCheatOnceCheated() {
        Assert.assertEquals(Move.COOPERATE, grudgeBehaviour.move());

        this.grudgeBehaviour.notifyOf(Move.CHEAT, Move.COOPERATE);
        Assert.assertEquals(Move.CHEAT, grudgeBehaviour.move());

        this.grudgeBehaviour.notifyOf(Move.COOPERATE, Move.COOPERATE);
        Assert.assertEquals(Move.CHEAT, grudgeBehaviour.move());

        this.grudgeBehaviour.notifyOf(Move.COOPERATE, Move.COOPERATE);
        Assert.assertEquals(Move.CHEAT, grudgeBehaviour.move());
    }
}
