package com.bootcamp.evolutionoftrust;
import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class PlayerTest {

    Player player = new Player();

    @Test
    public void playerMakesACooperateMove() {
        player = new Player(Behaviour.normal(new Scanner(String.valueOf(Move.COOPERATE))));
        Assert.assertEquals(Move.COOPERATE, player.move());
    }

    @Test
    public void playerMakesACheatMove() {
        player = new Player(Behaviour.normal(new Scanner(String.valueOf(Move.CHEAT))));
        Assert.assertEquals(Move.CHEAT, player.move());
    }

    @Test
    public void playerAlwaysMakeACheatMove() {
        player = new Player(Behaviour.cheat());
        Assert.assertEquals(Move.CHEAT, player.move());
    }

    @Test
    public void playerAlwaysCooperates() {
        player = new Player(Behaviour.cooperate());
        Assert.assertEquals(Move.COOPERATE, player.move());
    }

    @Test
    public void playerGainScore() {
        player.updateScore(3);
        Assert.assertEquals(3, player.score());
        player.updateScore(2);
        Assert.assertEquals(5, player.score());
    }


    @Test
    public void playerLoseScore() {
        player.updateScore(-3);
        Assert.assertEquals(-3, player.score());
        player.updateScore(-2);
        Assert.assertEquals(-5, player.score());
    }

    @Test
    public void playerCopyOtherPlayerMove() {
        // Arrange
        ObserveBehaviour copyBehaviour = Behaviour.copy();
        Player copyPlayer = new Player(copyBehaviour);

        // Act
        copyBehaviour.notifyOf(Move.COOPERATE, Move.CHEAT);

        // Asserts
        Assert.assertEquals(Move.CHEAT, copyPlayer.move());
//        Mockito.verify(copyBehaviour).notifyOf(Move.COOPERATE, Move.CHEAT);
    }

}
